# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This program is used to configure ICS settings in Linux platform.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

用法如下：
$ sudo ./netrt -d enable -s slave -w eth0 -t tun0 -g 10.7.0.1  -l 8.8.8.8/32,55.5.5.5/32
配置设备为slave模式，原有网关接口是eth0，新网关接口是tun0，新网关的IP是 “10.7.0.1”。把8.8.8.8/32, 55.5.5.5/32加入排除列表（仍然从原有网关接口 eth0收发数据）。
该指令执行完成后默认网关是tun0。

$ sudo ./netrt -d disable -s slave -w eth0 -t tun0 -g 10.7.0.1  -l 8.8.8.8/32,55.5.5.5/32
上述的撤销动作

$ sudo ./netrt -d enable -s master -w eth0 -t tun0
配置设备为master模式，原有网关接口是eth0，新网关接口是tun0。


$ sudo ./netrt -d disable -s master -w eth0 -t tun0
上述的撤销动作 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact