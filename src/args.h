/**
  args.h
 */

#ifndef ARGS_H
#define ARGS_H

#include <stdint.h>

#define MAX_MTU 9000

typedef enum {
	MODE_SERVER = 0,
	MODE_CLIENT = 1
} server_mode;

typedef struct {
	server_mode mode;

	const char *wan_intf;
	const char *tun_intf;
	const char *tun_gw;

	struct ip_node {
		char ip[16];
		int prefix_length;
		struct ip_node *next;
	} *ip_list_head;
} options_t;

#endif
