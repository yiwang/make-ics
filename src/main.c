/**
  args.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include "args.h"
#include "ifc.h"
#include "log.h"

static const char *help_message =
		"This program is used to modify default routes, and enable or disable "
		"ICS(internet-connection-sharing) feature to the tunnel interface.\n"
		"usage: netrt -d <enable/disable> -s <server/client> -w <wan interface> -t <tunnel interface> -g <tunnel gateway> -l <ip list>\n"
		"\n"
		"for instance: netrt -d enable -s server -w eth0 -t tun0 -g 10.7.0.1 -l 8.8.8.8/32,4.4.4.4\n"
		"\n"
		"  -h, --help            show this help message and exit\n"
		"  -d <enable/disable>   enable or disable ICS feature. enable is default.\n"
		"  -s <master/slave>     server type: master or slave. master is default.\n"
		"  -w <wan interface>    for instance: eth0\n"
		"  -t <tunnel interface> for instance: tun0, ipop0\n"
		"  -g <tunnel gateway>   for instance: 10.7.0.1\n"
		"  -l <ip list> ...      ip list which will not route to the tunnel. for instance: 8.8.8.8/32,207.9.8.2\n"
		"  -v                    verbose logging\n"
		"\n";

static void print_help()
{
	printf("%s", help_message);
	exit(1);
}

int execute(const char *cmd_line, int quiet)
{
	int pid,
	status,
	rc;

	const char *new_argv[4];
	new_argv[0] = "/bin/sh";
	new_argv[1] = "-c";
	new_argv[2] = cmd_line;
	new_argv[3] = NULL;

	logf("EXEC: %s", cmd_line);

	pid = fork();
	if (pid == 0) {    /* for the child process:         */
		/* We don't want to see any errors if quiet flag is on */
		if (quiet) close(2);
		if (execvp("/bin/sh", (char *const *)new_argv) == -1) {    /* execute the command  */
			errf("execvp(): %s", strerror(errno));
		} else {
			errf("execvp() failed");
		}
		exit(1);
	}

	/* for the parent:      */
	debugf("Waiting for PID %d to exit", pid);
	rc = waitpid(pid, &status, 0);
	debugf("Process PID %d exited", rc);

	return (WEXITSTATUS(status));
}

int parse_ip_list(options_t *args, const char *list)
{
	struct ip_node *ip;
	const char *s0 = list, *s1 = list;
	int count = 0;

	while (*s1 != '\0') {
		char *s2 = NULL;
		int prefix_length = 32;

		s1 = s0;
		while (*s1 != '\0') {
			if (*s1 == ',') {
				break;
			}
			if (*s1 == '/') {
				prefix_length = strtol(s1+1, &s2, 10);
				break;
			}
			s1++;
		}

		if (s1 - s0 < 7 || s1 - s0 > 15) { // strlen("8.8.8.8") == 7
			break;
		}

		ip = (struct ip_node *)malloc(sizeof(struct ip_node));
		if (ip == NULL) {
			break;
		}

		memcpy(ip->ip, s0, s1-s0);
		ip->ip[s1-s0] = '\0';
		ip->prefix_length = prefix_length;
		ip->next = args->ip_list_head;
		args->ip_list_head = ip;
		count++;

		s0 = s2 ? s2 + 1 : s1 + 1;
	}

	return count;
}

int route_enable(options_t *args)
{
	if (args->mode == MODE_SERVER) {
		/*
		 *
		iptables -t nat -A POSTROUTING -o $gw_intf -m comment --comment "$gw_intf (shadowvpn)" -j MASQUERADE

		iptables -A FORWARD -i $gw_intf -o $intf -m state --state RELATED,ESTABLISHED -j ACCEPT
		iptables -A FORWARD -i $intf -o $gw_intf -j ACCEPT

		# turn on MSS fix
		# MSS = MTU - TCP header - IP header
		iptables -t mangle -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
		 */

		char cmd[200];

		snprintf(cmd, sizeof(cmd),
				"iptables -t nat -A POSTROUTING -o %s -m comment --comment \"%s (tandem)\" -j MASQUERADE",
				args->wan_intf, args->wan_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -A FORWARD -i %s -o %s -m state --state RELATED,ESTABLISHED -j ACCEPT",
				args->wan_intf, args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -A FORWARD -i %s -o %s -j ACCEPT",
				args->tun_intf, args->wan_intf);
		execute(cmd, 0);
	}
	else {
		// iptables -t nat -A POSTROUTING -o $intf -j MASQUERADE
		// iptables -I FORWARD 1 -i $intf -m state --state RELATED,ESTABLISHED -j ACCEPT
		// iptables -I FORWARD 1 -o $intf -j ACCEPT

		char cmd[200];

		snprintf(cmd, sizeof(cmd),
				"iptables -t nat -A POSTROUTING -o %s -j MASQUERADE", args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -I FORWARD 1 -i %s -m state --state RELATED,ESTABLISHED -j ACCEPT",
				args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -I FORWARD 1 -o %s -j ACCEPT", args->tun_intf);
		execute(cmd, 0);

		int gw = ifc_get_default_route(args->wan_intf);
		if (gw == 0) {
			errf("route_enable: no default gw for %s", args->wan_intf);
			return -1;
		}

		// apply routes
		const struct ip_node *node;
		for (node = args->ip_list_head; node != NULL; node = node->next) {
			ifc_add_route(
					args->wan_intf,
					node->ip,
					node->prefix_length,
					inet_ntoa(*(struct in_addr *)&gw));
		}

		if (args->tun_gw == NULL) {
			errf("route_enable: no default gw for %s", args->tun_intf);
			return -1;
		}

		ifc_add_route(
				args->tun_intf,
				"0.0.0.0",
				0,
				args->tun_gw);
	}

	return 0;
}

int route_disable(options_t *args)
{
	if (args->mode == MODE_SERVER) {
		/*
		# turn off NAT over gw_intf and VPN
		iptables -t nat -D POSTROUTING -o $gw_intf -m comment --comment "$gw_intf (shadowvpn)" -j MASQUERADE
		iptables -D FORWARD -i $gw_intf -o $intf -m state --state RELATED,ESTABLISHED -j ACCEPT
		iptables -D FORWARD -i $intf -o $gw_intf -j ACCEPT

		# turn off MSS fix
		# MSS = MTU - TCP header - IP header
		iptables -t mangle -D FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
		 * */
		char cmd[200];

		snprintf(cmd, sizeof(cmd),
				"iptables -t nat -D POSTROUTING -o %s -m comment --comment \"%s (tandem)\" -j MASQUERADE",
				args->wan_intf, args->wan_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -D FORWARD -i %s -o %s -m state --state RELATED,ESTABLISHED -j ACCEPT",
				args->wan_intf, args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -D FORWARD -i %s -o %s -j ACCEPT",
				args->tun_intf, args->wan_intf);
		execute(cmd, 0);
	}
	else {
		// iptables -t nat -A POSTROUTING -o $intf -j MASQUERADE
		// iptables -I FORWARD 1 -i $intf -m state --state RELATED,ESTABLISHED -j ACCEPT
		// iptables -I FORWARD 1 -o $intf -j ACCEPT

		char cmd[200];

		snprintf(cmd, sizeof(cmd),
				"iptables -t nat -D POSTROUTING -o %s -j MASQUERADE", args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -D FORWARD -i %s -m state --state RELATED,ESTABLISHED -j ACCEPT",
				args->tun_intf);
		execute(cmd, 0);

		snprintf(cmd, sizeof(cmd),
				"iptables -D FORWARD -o %s -j ACCEPT", args->tun_intf);
		execute(cmd, 0);

		int gw = ifc_get_default_route(args->wan_intf);
		if (gw == 0) {
			errf("route_disable: no default gw for %s", args->wan_intf);
			return -1;
		}

		// apply routes
		const struct ip_node *node;
		for (node = args->ip_list_head; node != NULL; node = node->next) {
			ifc_remove_route(
					args->wan_intf,
					node->ip,
					node->prefix_length,
					inet_ntoa(*(struct in_addr *)&gw));
		}

		if (args->tun_gw == NULL) {
			errf("route_disable: no default gw for %s", args->tun_intf);
			return -1;
		}

		ifc_remove_route(
				args->tun_intf,
				"0.0.0.0",
				0,
				args->tun_gw);
	}
	return 0;
}

int args_parse(options_t *args, int argc, char **argv)
{
	int enable = 1;
	char *list = NULL;
	int ch;

	memset(args, 0, sizeof(options_t));
	args->mode = MODE_SERVER;
	while ((ch = getopt(argc, argv, "hd:s:w:t:g:l:v")) != -1) {
		switch (ch) {
		case 'd':
			if (strcmp("enable", optarg) == 0) {
				enable = 1;
			}
			else if (strcmp("disable", optarg) == 0) {
				enable = 0;
			}
			else {
				errf("unknown command %s", optarg);
				print_help();
			}
			break;
		case 's':
			if (strcmp("master", optarg) == 0)
				args->mode = MODE_SERVER;
			else if (strcmp("slave", optarg) == 0)
				args->mode = MODE_CLIENT;
			else {
				errf("unknown command %s", optarg);
				print_help();
			}
			break;
		case 'w':
			args->wan_intf = strdup(optarg);
			break;
		case 't':
			args->tun_intf = strdup(optarg);
			break;
		case 'g':
			args->tun_gw = strdup(optarg);
			break;
		case 'l':
			list = optarg;
			break;
		case 'v':
			verbose_mode = 1;
			break;
		default:
			print_help();
		}
	}

	if (args->wan_intf == NULL || args->tun_intf == NULL) {
		print_help();
		return -1;
	}

	if (list != NULL) {
		parse_ip_list(args, list);
	}
	return enable ? route_enable(args) : route_disable(args);
}

int main(int argc, char **argv)
{
	options_t args;
	return args_parse(&args, argc, argv);
}
