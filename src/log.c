/**
  log.c
 */

#include <stdio.h>
#include <string.h>
#include <time.h>

int verbose_mode = 0;

void log_timestamp(FILE *out) {
	time_t now;
	time(&now);
	char *time_str = ctime(&now);
	time_str[strlen(time_str) - 1] = '\0';
	fprintf(out, "%s ", time_str);
}

void perror_timestamp(const char *msg, const char *file, int line) {
	log_timestamp(stderr);
	fprintf(stderr, "%s:%d ", file, line);
#ifdef TARGET_WIN32
	LPVOID *err_str = NULL;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, WSAGetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &err_str, 0, NULL);
	if (err_str != NULL) {
		fprintf(stderr, "%s: %s\n", msg, (char *)err_str);
		LocalFree(err_str);
	}
#else
	perror(msg);
#endif
}
